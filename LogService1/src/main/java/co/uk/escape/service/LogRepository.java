package co.uk.escape.service;

import org.springframework.data.mongodb.repository.MongoRepository;

import co.uk.escape.domain.Log;
import co.uk.escape.domain.RegisteredUser;

public interface  LogRepository extends MongoRepository<Log, String>{

}

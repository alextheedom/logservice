package co.uk.escape.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import co.uk.escape.domain.Log;
import co.uk.escape.domain.RegistrationRequest;

@Controller
public class ReceiverLog {
	
	@Autowired
	LogRepository logRepository;
	
	public void saveLog(RegistrationRequest newUserRegistrationRequest) {
		
		Log log = new Log(newUserRegistrationRequest);		
	
		try {
			log = logRepository.save(log);
		} catch (Exception e){
			System.out.println("opps problems: " + e.getMessage());
		}

        System.out.println("[1] ReceiverLog <" + log + ">");
    }
	
}

package co.uk.escape.domain;

import java.io.Serializable;
import java.util.Date;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "logs_loadtest")
public class Log implements Serializable{
	
	private static final long serialVersionUID = -8687410524098828064L;
	
	private String logEntry;

	public Log(RegistrationRequest newUserRegistrationRequest) {
		this.setLogEntry(new Date().toString() + " [New user registration request] : " + newUserRegistrationRequest.toString());
	}

	public String getLogEntry() {
		return logEntry;
	}

	public void setLogEntry(String logEntry) {
		this.logEntry = logEntry;
	}

	@Override
	public String toString() {
		return "Log [logEntry=" + logEntry + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((logEntry == null) ? 0 : logEntry.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Log other = (Log) obj;
		if (logEntry == null) {
			if (other.logEntry != null)
				return false;
		} else if (!logEntry.equals(other.logEntry))
			return false;
		return true;
	}
		

}
